# Episode 1 - Login

Halo Koders! Tim FE (_Frontend_) Kodingers di episode pertama ini bakal bahas tips n tricks tentang login menggunakan **Flexbox**.

## _Environment_

Pada episode ini, kami menggunakan _env_ sebagai berikut.
```
  note: Environment ini bersifat opsional, tidak wajib diinstall.
```

- Visual Studio Code versi terbaru
